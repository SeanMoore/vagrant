# Vagrant

This project provides a simple way to handle multiple Vagrant configurations without even adding them to this repository.

## Setup

Bring the folder of the projects into this folder that you want. For example:

```bash
$ ls Vagrantfile */Vagrantfile
Vagrantfile
00_vagrant_cache/Vagrantfile
01_vagrant_my_machine/Vagrantfile
$ 
```

Run bring up on all the machines.

```bash
$ vagrant up
```

## Strong Suggestions

- Top-level folders matching the regex ```\d+_vagrant_.*``` and containing a Vagrantfile are considered machines.
- Machines are enumerated in the same order as the order when their folders are lexically sorted. So multi-machine commands that don't have a machine specified will be executed in that order.
  - Note that lexical ordering allows for orderings like this ```["01_b", "0_a"]```. So you *should* rename your folders to use the same number of leading digits.
- When provisioning a machine without specifying the provisioner the ```provision.sh``` script in the machine folder will be run with each of the folders matching the regex string ```"#{MACHINE_FOLDER}/provision/\\d+_.*"``` with a reload of the machine being done after successfully executing all the contents of the folder.

